#!/usr/bin/env python
# coding: utf-8

# In[3]:


#SOAL 01
class University:
    
    def __init__(self,university_name, school_type):
        self.name = university_name
        self.school_type = school_type
        
    def describe_university(self):
        print("The University Name Is {}".format(self.name))
        print("The School Type Is {}".format(self.school_type))
        return " "
        
    def open_university(self,opentime):
        self.opentime = opentime
        print("This University Open from {}".format(opentime))
        return " "
        
university = University("Prasmul","Swasta")
print(university.name)
print(university.school_type)
print(university.describe_university())
print(university.open_university("Monday Until Friday"))

IPB = University("IPB","Negeri")
print(IPB.describe_university())

ITB = University("IPB","Negeri")
print(ITB.describe_university())

UGM = University("IPB","Negeri")
print(UGM.describe_university())


# In[ ]:




