#!/usr/bin/env python
# coding: utf-8

# In[22]:


#SOAL 04
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import datetime
get_ipython().run_line_magic('matplotlib', 'inline')
tesla = pd.read_csv("Tesla_Stock.csv", index_col="Date",parse_dates=True)
Ford = pd.read_csv("Ford_Stock.csv", index_col="Date",parse_dates=True)
GM = pd.read_csv("GM_Stock.csv",index_col="Date",parse_dates=True)


# In[32]:


fig, ax = plt.subplots(figsize=(20,10))
ax.set_xlim([datetime.date(2012, 1, 1), datetime.date(2017, 1, 1)])
ax.xaxis.set_major_locator(dates.MonthLocator(bymonth=(7,1)))
ax.xaxis.set_major_formatter(dates.DateFormatter("%Y-%m"))
ax.plot(Ford['Open'],color= 'green',label="Ford")
ax.plot(tesla['Open'],color='blue',label="Tesla")
ax.plot(GM['Open'],color='orange',label="GM")
ax.set_xlabel("Date")
ax.set_title("Open Price")
ax.legend()
fig.autofmt_xdate()


# In[ ]:




