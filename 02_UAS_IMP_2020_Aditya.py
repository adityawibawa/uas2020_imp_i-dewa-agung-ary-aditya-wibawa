#!/usr/bin/env python
# coding: utf-8

# In[3]:


#SOAL 02
class Student:
    
    def __init__(self,first_name, last_name, address, major):
        self.firstname = first_name
        self.lastname = last_name
        self.address = address
        self.major = major
        
    def describe_student(self):
        print("My name is {} {}".format(self.firstname,self.lastname))
        print("I live in {}".format(self.address))
        print("And I student from {} Major".format(self.major))
        return " "
        
    def greet_student(self):
        print("Hai, How are you? Hopefully youre have a big spirit today!")
        return " "
        
adit = Student("Aditya","Wibawa","BSD","Software Engineering")

print(adit)
print(adit.describe_student())
print(adit.greet_student())


# In[ ]:




